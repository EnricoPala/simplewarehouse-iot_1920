package controller;

import java.util.*;

public class Prova {

	private static String[] systemState = {"WarehouseStopped","WarehouseResumed","WarehouseRestarted"};
	private static String[] requestGet = {"Check","Overflow"};
	private static int size = 3;
	private int[] typeOfWares = {1,2,3}; // tipologia di merce
	private int[] totalWares = new int[this.size]; //array che mi tiene traccia di tutte le merci
	private Map<String,Integer> map = new HashMap<>();
	private String state = "nothing";
	
	
	public void setTotalWares(int type) {
		int value = this.returnType(type);
		String key = this.returnStringOfWare(type);
		this.totalWares[value-1]++;
		this.map.put(key, value);
		
	}
	
	
	private String returnStringOfWare(int type) {
		if(type == 1) {
			return "A";
		}else if(type == 2){
			return "B";
		}else {
			return "C";
		}
	}


	private int returnType(int num) {
		int value = 0;
		for (int i = 0; i < typeOfWares.length; i++) {
			if(num == typeOfWares[i]) {
				value = num;
			}
		}
		return value;
		
	}
	
	public  Map<String, Integer> getTotalWares() {
		return this.map;
	}
	
	
	//ritorna array
	public static String[] getSystemState() {
		return systemState;
	}

	public static String[] getRequestGet() {
		return requestGet;
	}
}