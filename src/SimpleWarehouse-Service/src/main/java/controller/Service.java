package controller;

import java.util.*;

public class Service {

	private  String[] systemState = {"WarehouseStopped","WarehouseResumed","WarehouseRestarted"};
	private  String[] requestGet = {"Check","Overflow"};
	private  String[] webSiteState = {"STOP","RESUME","CLEAR"};
	private int size = 3;
	private int[] typeOfWares = {1,2,3}; // tipologia di merce array di ppoggio
	private int[] totalWares = new int[this.size]; //array che mi tiene traccia di tutte le merci
	private Map<String,Integer> map = new HashMap<>();
	private String state = "Available";
	

	/*Array json da passare*/
	public void setTotalWares(int type) {
		int value = this.returnType(type);
		String key = this.returnStringOfWare(type);
		this.totalWares[value-1]++;
		this.map.put(key, this.totalWares[value-1]);
		
	}
	
	/*ritorna il  nome della merce A , B, C in modo poco pulito*/
	private String returnStringOfWare(int type) {
		if(type == 1) {
			return "A";
		}else if(type == 2){
			return "B";
		}else {
			return "C";
		}
	}

	/*ritorna se la merce è 1 2 3 per incrementare il numero*/
	private int returnType(int num) {
		int value = 0;
		for (int i = 0; i < typeOfWares.length; i++) {
			if(num == typeOfWares[i]) {
				value = num;
			}
		}
		return value;
		
	}
	
	public  Map<String, Integer> getTotalWares() {
		return this.map;
	}
	
	
	//ritorna array degli stati del Warehouse
	public  String[] getSystemState() {
		return this.systemState;
	}

	//ritorna path delle richieste get al server da parte dell'esp
	public  String[] getRequestGet() {
		return this.requestGet;
	}
	
	//setta lo stato del magazzino
	public void setState(String string) {
		this.state = string;
	}
	
	
	public String getState() {
		return this.state;
	}
	
	// i due path per le richieste da parte del sito web
	public String[] getWebSiteState() {
		return this.webSiteState;
	}
	
	public void restart() {
		this.map.clear();
		this.totalWares[0] = 0 ;
		this.totalWares[1] = 0 ;
		this.totalWares[2] = 0 ;
	}
}
