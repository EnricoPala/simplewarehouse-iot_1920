package comm.service.communication;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import controller.Service;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;

public class SimpleWarehouseServer extends AbstractVerticle {

	static int toPreventRestartedBug = 0;


	public static void main(String[] args) {
		
		
		Service service = new Service();
		Vertx vertx = Vertx.vertx();
		HttpServer server = vertx.createHttpServer();
		Router router = Router.router(vertx);
		
		/*Per evitare il block CORS dei siti web*/
		Set<String> allowedHeaders = new HashSet<>();
		allowedHeaders.add("x-requested-with");
		allowedHeaders.add("Access-Control-Allow-Origin");
		allowedHeaders.add("origin");
		allowedHeaders.add("Content-Type");
		allowedHeaders.add("accept");
		allowedHeaders.add("X-PINGARUNER");
		Set<HttpMethod> allowedMethods = new HashSet<>();
		allowedMethods.add(HttpMethod.GET);
		router.route().handler(CorsHandler.create("*").allowedHeaders(allowedHeaders).allowedMethods(allowedMethods));
		
		

		
		router.route(HttpMethod.GET,"/checkState/:value").handler(routingContext->{
			
			 HttpServerResponse response = routingContext.response();
			 String value = routingContext.request().getParam("value");
			 String[] stateRequest = service.getRequestGet();
			 
		
			 if(value.equals(stateRequest[1])) {
				 service.setState("WarehouseStopped");
			 }
			  
			 if(service.getState().equals("WarehouseRestarted")) {
				 if(toPreventRestartedBug == 0) {
					 toPreventRestartedBug++;
					 service.restart();
				 }else {
					 service.setState("Available");
					 toPreventRestartedBug = 0;
				 }
			 }
			 response.setStatusCode(200).putHeader("content-type", "application/json")
				.end(service.getState());
			
		});
		
		
	
		 router.route(HttpMethod.GET,"/getWeightWare/:value").handler(routingContext->{
			 HttpServerResponse response = routingContext.response();
			 String weight = routingContext.request().getParam("value");
			 String[] pathD = service.getRequestGet();
			 String[] state = service.getSystemState();
			
			 service.setTotalWares(Integer.parseInt(weight));
			 /*String per debug*/
			 String toSend = "Weight added";
			
			 System.out.println(service.getTotalWares());
			 response.setStatusCode(200).putHeader("content-type", "application/json")
				.end(toSend);
			 
		 });
		 	 
		 
		 
		 router.route(HttpMethod.GET,"/stateWarehouse/:value").handler(routingContext->{
			 HttpServerResponse response = routingContext.response();
			 /*Per debug*/
			 String stateLocal = null;
			 String value = routingContext.request().getParam("value");
			 String[] stateWeb = service.getWebSiteState(); // STOP CLEAR RESUME
			 String[] stateWare = service.getSystemState(); //WarehouseStopped,WarehouseResumed ecc
			 
			 /*STOP*/
			 if(value.equals(stateWeb[0])) {
				 stateLocal = stateWare[0];
				 service.setState(stateLocal);
				 /*RESUME*/
			 }else if(value.equals(stateWeb[1])) {
				 stateLocal = stateWare[1];
				 service.setState(stateLocal);
				 /*RESTART*/
			 }else if(value.equals(stateWeb[2])) {
				 stateLocal = stateWare[2];
				 service.setState(stateLocal);
			 }
			 
			 response.setStatusCode(200).putHeader("content-type", "application/json")
			 					.end("{\"Inserted_Value\":\"" + stateLocal + "\"}");
			
		 });
		 
		 
		 
		 router.route(HttpMethod.GET,"/getAll").handler(routingContext->{
			 HttpServerResponse response = routingContext.response();
			 Map<String, Integer> value = service.getTotalWares();
			 Gson gson = new GsonBuilder().setPrettyPrinting().create();
			 String toString = gson.toJson(value);
			// System.out.println(toString);
			 response.setStatusCode(200).putHeader("content-type", "application/json")
			 					.end( toString);
			 
			 
		 });
		 
		 router.route(HttpMethod.GET,"/toApp").handler(routingContext->{
			 HttpServerResponse response = routingContext.response();
		
			 Map<String, Integer> value = service.getTotalWares();
			 if(service.getState().equals("WarehouseStopped")) {
				 value.put("STATE", 400);
			 }else {
				 value.put("STATE", 200);
			 }
			 
			 Gson gson = new GsonBuilder().setPrettyPrinting().create();
			 String toString = gson.toJson(value);
	
			 response.setStatusCode(200).putHeader("content-type", "application/json")
			 					.end(toString);
			 
			 
		 });
		 
		server.requestHandler(router).listen(80);
		 System.out.println("PARTITO");
	  }

}



