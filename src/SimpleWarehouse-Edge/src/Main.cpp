#include <Arduino.h>
#include <esp8266wifi.h>
#include <esp8266httpclient.h>

#include "Led.h"
#include "Pot.h"

#define GREEN D6
#define RED D3
#define ANALOG_POT A0
#define DELTA 3000
#define MAXWEIGHT 10


const char* ssid = "name";
const char* password = "pass";
const String url = "http://ed360080.ngrok.io"; /*Service destination*/
const String pathToSendWeight = "/getWeightWare/";        
const String pathToCheckState = "/checkState/";

String stateWarehouse;
unsigned long previousTime = millis();
int currentPot;
int totalWeight = 0;

Light* availableLed;
Light* notAvailableLed;
Potentiometer* pot;

int preventStartBug = 0;

enum {AVAILABLE,NOTAVAILABLE} state;



String sendPackage(String stringToSend,String path)
{
   HTTPClient http;                       //Declare an object of class HTTPClient
   http.begin(url + path + stringToSend); //Specify request destination
   int httpCode = http.GET();             //Send the request
   if (httpCode > 0)
   {                                     //Check the returning code
      String payload = http.getString(); //Get the request response payload
      http.end();
      return payload;
   }
   else
   {
      return "Error";
   }
}


bool checkTotalWeight(int weight)
{
   totalWeight = totalWeight + weight;
   return totalWeight < MAXWEIGHT;
}


bool checkTime(unsigned long curr)
{
   return (curr - previousTime) >= DELTA;
}

void availableStep(){
   availableLed->switchOn();
   notAvailableLed->switchOff();
   int newValue = pot->getValue();

   /*prima che parta il server mi legge già il pot, quindi faccio questa operazione per evitare il bug*/
   if(preventStartBug == 0){
      preventStartBug =1;
       currentPot = newValue;
   }

   /*controllo periodicamente lo stato del magazzino */
   String check = sendPackage("Check",pathToCheckState);
  // Serial.println(check);
   if(check == "WarehouseStopped")
   {
      state = NOTAVAILABLE;
   }else{

      /* solo se il potenziometro cambia allora posso inserire il nuovo peso*/
      if (newValue != currentPot)
      {
         /*se il potenziometro è minore della soglia*/
         if (checkTotalWeight(newValue))
         {
            String weight = sendPackage(String(newValue),pathToSendWeight);
            delay(100);
            //Serial.print(weight);
            Serial.println(totalWeight);
            currentPot = newValue;
         }
         else
         {
           /*Invio due volte altrimenti perdo un valore*/
            String weight = sendPackage(String(newValue),pathToSendWeight);

            /*se overflow fermo il magazzino e cambio stato*/
            String overflow = sendPackage("Overflow",pathToCheckState);
            Serial.println(overflow);
            state = NOTAVAILABLE;
         }
      }
   }
}

void notAvailableStep(){
   availableLed->switchOff();
   notAvailableLed->switchOn();
   String check = sendPackage("Check",pathToCheckState);
   if(check == "WarehouseResumed"){
      state = AVAILABLE;
   }else if(check == "WarehouseRestarted"){
      int newValue = pot->getValue();
      state = AVAILABLE;
      totalWeight = 0;
      currentPot = newValue;
   }
}


void setup()
{
   delay(1000);
   Serial.begin(9600);
   WiFi.mode(WIFI_STA);
   WiFi.begin(ssid, password);

   while (WiFi.status() != WL_CONNECTED){delay(500); /*Serial.print("Connecting..");*/}

   availableLed = new Led(GREEN);
   notAvailableLed = new Led(RED);
   pot = new Potentiometer(ANALOG_POT);
   state = AVAILABLE;
}


void loop()
{
   unsigned long currentTime = millis();

   if (checkTime(currentTime))
   {
      if(state == AVAILABLE){
         availableStep();
      }else{
         notAvailableStep();
      }
      previousTime = millis();
   }

   if(currentTime < previousTime)
   {
      previousTime = currentTime;
   }
}