#ifndef __BLINKTASK__
#define __BLINKTASK__

#include "Task.h"
#include "Led.h"
#include "TaskButtonA.h"
#include "TaskButtonC.h"
#include "TaskButtonB.h"

class ControlTaskBt: public Task {

  int rxd_pin;
  int txd_pin;
  TaskButtonA* buttonA;
  TaskButtonB* buttonB;
  TaskButtonC* buttonC;
  
  enum { WAIT_FOR_CONNECTION, CONNECTED} state;

public:

  ControlTaskBt(int rxd_pin,int txd_pin,  TaskButtonA* buttonA,TaskButtonB* buttonB,TaskButtonC* buttonC);  
  void init(int period);  
  void tick();
};

#endif
