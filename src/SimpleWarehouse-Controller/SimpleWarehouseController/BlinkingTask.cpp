#include "BlinkingTask.h"
#include "Arduino.h"
#include "Led.h"


BlinkingTask::BlinkingTask(int pin){ 
  this->pin = pin;
  this-> pLed = new Led(pin);
}

void BlinkingTask::init(){
  pLed = new Led(pin);
}
void BlinkingTask::setActive(bool active){
  Task::setActive(active);
  if (active){
    state = ON;
  } else {
    pLed->switchOff();
  }
}
  
void BlinkingTask::tick(){
  switch (state){
    case ON: {
      pLed->switchOn();
      state = OFF;
      break;
    }
    case OFF: {
      pLed->switchOff();
      state = ON;
      break;
    }
  }
}
