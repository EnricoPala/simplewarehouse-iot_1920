#ifndef __BLINKINGTASK__
#define __BLINKINGTASK__

#include "Task.h"
#include "Light.h"

class BlinkingTask: public Task {
  
public:
  BlinkingTask(int pin);  
  void setActive(bool active);
  void tick();
  void init();

private:

  long ts0;  
  int pin;
  Light* pLed;
  enum { INIT, ON, OFF } state;

};

#endif
