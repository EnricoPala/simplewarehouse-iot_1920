#include "Arduino.h"
#include "TaskButtonB.h"
#include "BlinkingTask.h"

long counterB;
int deltaB = 5000;

TaskButtonB::TaskButtonB(Task* task, Task* task1){
  this->blinkLedB = task;
  this->servoMove = task1;
}

void TaskButtonB::init(int period){
  Task::init(period);
  this->blinkLedB->setActive(false);
  this->servoMove->setActive(false);
  state = INIT;
}

void TaskButtonB::tick(){

  switch(state){

    case INIT:
    counterB = millis();
    state = RUNNING;
    break;
    
    case RUNNING:
    blinkLedB->setActive(true);
    servoMove->setActive(true);
    if(millis()-counterB > deltaB){
      state = END;
    }
    break;

    case END:
    blinkLedB->setActive(false);
    servoMove->setActive(false);
    setCompleted();
    state = INIT;
    break;
  }
}
