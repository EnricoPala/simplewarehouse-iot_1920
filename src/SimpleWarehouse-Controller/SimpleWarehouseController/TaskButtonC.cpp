#include "Arduino.h"
#include "TaskButtonC.h"
#include "BlinkingTask.h"

long counterC;
int deltaC = 5000;

TaskButtonC::TaskButtonC(Task* task, Task* task1){
  this->blinkLedC = task;
  this->servoMove = task1;
}

void TaskButtonC::init(int period){
  Task::init(period);
  this->blinkLedC->setActive(false);
  this->servoMove->setActive(false);
  state = INIT;
}

void TaskButtonC::tick(){

  switch(state){

    case INIT:
    counterC = millis();
    state = RUNNING;
    break;
    
    case RUNNING:
    blinkLedC->setActive(true);
    this->servoMove->setActive(true);
    if(millis()-counterC > deltaC){
      state = END;
    }
    break;

    case END:
    blinkLedC->setActive(false);
    this->servoMove->setActive(false);
    setCompleted();
    state = INIT;
    break;
  }
}
