#ifndef __SERVOTASK__
#define __SERVOTASK__

#include "Task.h"

class ServoTask: public Task {

  int servoPin;

  enum { OPENING,OPEN,CLOSING} state;

public:

  ServoTask(int pin);  
  void setActive(bool active);
  void tick();
};

#endif
