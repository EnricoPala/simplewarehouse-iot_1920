#include <Arduino.h>


#include "ControlTaskBt.h"
#include "TaskButtonA.h"
#include "TaskButtonB.h"
#include "TaskButtonC.h"
#include "Scheduler.h"
#include "Value.h"

Scheduler sched;

void setup(){

   Task* servoTask = new ServoTask(SERVO_PIN);
   servoTask->setActive(false);
   servoTask->init(SUB_TASK);
   
   Task* blinkLedA = new BlinkingTask(LED_A);
   blinkLedA->init(SUB_TASK);
   blinkLedA->setActive(false);

   Task* blinkLedB = new BlinkingTask(LED_B);
   blinkLedB->init(SUB_TASK);
   blinkLedB->setActive(false);

   Task* blinkLedC = new BlinkingTask(LED_C);
   blinkLedC->init(SUB_TASK);
   blinkLedC->setActive(false);
   
   Task* taskButtonA = new TaskButtonA(blinkLedA,servoTask);
   taskButtonA->init(TASK);
   taskButtonA->setActive(false);

   Task* taskButtonB = new TaskButtonB(blinkLedB,servoTask);
   taskButtonB->init(TASK);
   taskButtonB->setActive(false);

   Task* taskButtonC = new TaskButtonC(blinkLedC,servoTask);
   taskButtonC->init(TASK);
   taskButtonC->setActive(false);
   
   Task* controlTaskBt = new ControlTaskBt(RXD_PIN,TXD_PIN, taskButtonA,taskButtonB,taskButtonC);
   controlTaskBt->init(BT_TASK);
   
   sched.init(SCHEDULER);
  
    Serial.begin(9600);

   sched.addTask(controlTaskBt);
   sched.addTask(taskButtonA);
   sched.addTask(blinkLedA);
   sched.addTask(taskButtonB);
   sched.addTask(blinkLedB);
   sched.addTask(taskButtonC);
   sched.addTask(blinkLedC);
   sched.addTask(servoTask);

}

void loop(){
  sched.schedule();
}
