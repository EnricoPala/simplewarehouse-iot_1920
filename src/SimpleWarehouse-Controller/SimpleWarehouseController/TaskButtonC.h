#ifndef __TASKBUTTONC__
#define __TASKBUTTONC__

#include "Task.h"
#include "BlinkingTask.h"
#include "ServoTask.h"

class TaskButtonC: public Task{

  BlinkingTask* blinkLedC;
  ServoTask* servoMove;
  enum{INIT,RUNNING,END}state;
  
public:
  TaskButtonC(Task* task, Task* task1);  
  void init(int period);
  void tick();
};

#endif
