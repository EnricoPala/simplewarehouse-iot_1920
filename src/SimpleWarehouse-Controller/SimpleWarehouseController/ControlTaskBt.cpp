#include "ControlTaskBt.h"
#include "Arduino.h"
#include "MsgService.h"
#include "TaskButtonA.h"



MsgService* msgService;

ControlTaskBt::ControlTaskBt(int rxd_pin, int txd_pin, TaskButtonA* buttonA, TaskButtonB* buttonB, TaskButtonC* buttonC){
  this->rxd_pin = rxd_pin;   
  this->txd_pin = txd_pin;    
  this->buttonA = buttonA;
  this->buttonB = buttonB;
  this->buttonC = buttonC;
};
  
void ControlTaskBt::init(int period){
  Task::init(period);
   msgService = new MsgService(rxd_pin,txd_pin);
  msgService->init();  
  state = WAIT_FOR_CONNECTION;    
};
  
void ControlTaskBt::tick(){
  switch (state){
    case WAIT_FOR_CONNECTION:
      buttonA -> setActive(false);
      buttonB -> setActive(false);
      buttonC -> setActive(false);
      state = CONNECTED;
      break;
      
    case CONNECTED:
    if(msgService->isMsgAvailable()){
         Msg* message = msgService->receiveMsg();
        String msg = message->getContent();
        if(msg == "A"){
          buttonA->setActive(true);
        }else if(msg == "B"){
         buttonB -> setActive(true);
        }else if(msg == "C"){
         buttonC -> setActive(true);
        }
       }
      break;
  }
}
