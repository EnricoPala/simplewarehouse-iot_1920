#ifndef __TASKBUTTONB__
#define __TASKBUTTONB__

#include "Task.h"
#include "BlinkingTask.h"
#include "ServoTask.h"

class TaskButtonB: public Task{

  BlinkingTask* blinkLedB;
  ServoTask* servoMove;
  enum{INIT,RUNNING,END}state;
  
public:
  TaskButtonB(Task* task, Task* task1);  
  void init(int period);
  void tick();
};

#endif
