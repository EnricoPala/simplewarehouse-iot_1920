#include "Arduino.h"
#include "ServoTask.h"
#include "ServoTimer2.h"

long servotimer;
int opening = 1500;
int wait = 2000;


 ServoTimer2 serv; 
ServoTask::ServoTask(int pin){
  this->servoPin = pin;
  serv.attach(servoPin);
 }

void ServoTask::setActive(bool active){
  Task::setActive(active);
  serv.write(750);
  if (active){
    state = OPENING;
    servotimer=millis();
  }
}

void ServoTask:: tick(){

  switch(state){
    case OPENING:
    serv.write(2250);
    if(millis()- servotimer > opening){
      state = OPEN;
      servotimer = millis();
    }
    break;

    case OPEN:
     if(millis()- servotimer > wait){
      servotimer = millis();
      state = CLOSING;
    }
    break;

    case CLOSING:
     if(millis()- servotimer < opening){
      serv.write(750);
    }
    break;

  }
}
