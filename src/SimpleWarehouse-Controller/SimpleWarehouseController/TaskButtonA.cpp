#include "Arduino.h"
#include "TaskButtonA.h"
#include "BlinkingTask.h"

long counter;
int delta = 5000;

TaskButtonA::TaskButtonA(Task* task, Task* task1){
  this->blinkLedA = task;
  this->servoMove = task1;
}

void TaskButtonA::init(int period){
  Task::init(period);
  this->blinkLedA->setActive(false);
  this->servoMove->setActive(false);
  state = INIT;
}

void TaskButtonA::tick(){

  switch(state){

    case INIT:
    counter = millis();
    state = RUNNING;
    break;
    
    case RUNNING:
    blinkLedA->setActive(true);
    servoMove->setActive(true);
    if(millis()-counter > delta){
      state = END;
    }
    break;

    case END:
    blinkLedA->setActive(false);
    servoMove->setActive(false);
    setCompleted();
    state = INIT;
    break;
  }
}
