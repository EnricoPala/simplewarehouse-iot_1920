#ifndef __TASKBUTTONA__
#define __TASKBUTTONA__

#include "Task.h"
#include "BlinkingTask.h"
#include "ServoTask.h"

class TaskButtonA: public Task{

  BlinkingTask* blinkLedA;
  ServoTask* servoMove;
  enum{INIT,RUNNING,END}state;
  
public:
  TaskButtonA(Task* task, Task* task1);  
  void init(int period);
  void tick();
};

#endif
