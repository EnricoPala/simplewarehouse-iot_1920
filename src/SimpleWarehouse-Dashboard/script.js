var url = "http://ed360080.ngrok.io";
var maxWeight = 100;
var deltaA = 10;
var deltaB = 20;
var deltaC = 30;
var currentWeight = 0 ;
var currA = 0;
var currB = 0;
var currC = 0;

window.setInterval(function(){
  var xhttp = new XMLHttpRequest();
  xhttp.open('GET', url+"/getAll",true);
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      console.log(JSON.parse(xhttp.responseText));
      // if response = warehouseStopped everything is ok
      var cell1 = document.getElementById('A');
      var cell2 = document.getElementById('B');
      var cell3 = document.getElementById('C');

      /*update every time the table */
      if(JSON.parse(xhttp.responseText).A != undefined){
        cell1.innerHTML = JSON.parse(xhttp.responseText).A;
          if(JSON.parse(xhttp.responseText).A != currA){
            currentWeight = currentWeight - currA*deltaA;
            currentWeight = currentWeight + JSON.parse(xhttp.responseText).A*deltaA;
            currA = JSON.parse(xhttp.responseText).A;
          }
        }else{
          cell1.innerHTML = "0";
        }

        if(JSON.parse(xhttp.responseText).B != undefined){
          cell2.innerHTML = JSON.parse(xhttp.responseText).B;
          if(JSON.parse(xhttp.responseText).B != currB){
            currentWeight = currentWeight - currB*deltaB;
            currentWeight = currentWeight + JSON.parse(xhttp.responseText).B*deltaB;
            currB = JSON.parse(xhttp.responseText).B;
          }
        }else{
          cell2.innerHTML = "0";
        }
        if(JSON.parse(xhttp.responseText).C != undefined){
          cell3.innerHTML = JSON.parse(xhttp.responseText).C;
          if(JSON.parse(xhttp.responseText).C != currC){
            /*bug fissato, ogni volta che ricevo un nuovo valore tolgo il precedente ma e somma il nuovo*/
            currentWeight = currentWeight - currC*deltaC;
            currentWeight = currentWeight + JSON.parse(xhttp.responseText).C*deltaC;
            currC = JSON.parse(xhttp.responseText).C;
          }
        }else{
          cell3.innerHTML = "0";
        }
      }
    };
    xhttp.send();


    /*case overflow*/
    if(currentWeight >= maxWeight){
      document.getElementById("total").innerHTML="Warehouse full!";
      document.getElementById('stop').disabled = true;
      document.getElementById('resume').disabled = true;
      document.getElementById('restart').disabled = false;
      document.getElementById("state").innerHTML="not available";
     }else{
      document.getElementById('restart').disabled = true;
      document.getElementById("total").innerHTML="Total weight: "+(currentWeight)+"/100";
    //  document.getElementById("state").innerHTML="Warehouse state: available";
    }
  },4000);


function stop(){
  var xhttp = new XMLHttpRequest();
 xhttp.open( "GET", url+"/stateWarehouse/STOP",true); // false for synchronous request
 xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
       // if response = warehouseStopped everything is ok
        console.log(xhttp.responseText);
    }
};
  xhttp.send();
  document.getElementById('stop').disabled = true;
  document.getElementById('restart').disabled = true;
  document.getElementById("state").innerHTML="not available";
}

function resume(){
var xhttp = new XMLHttpRequest();
xhttp.open( "GET", url+"/stateWarehouse/RESUME",true); // false for synchronous request
xhttp.onreadystatechange = function() {
  if (this.readyState == 4 && this.status == 200) {
     // if response = warehouseStopped everything is ok
      console.log(xhttp.responseText);
  }
};
xhttp.send();
document.getElementById('stop').disabled = false;
document.getElementById('restart').disabled = false;
document.getElementById("state").innerHTML="available";
}


function restart(){
  console.log("ENTRO");
var xhttp = new XMLHttpRequest();
xhttp.open( "GET", url+"/stateWarehouse/CLEAR",true); // false for synchronous request
xhttp.onreadystatechange = function() {
  if (this.readyState == 4 && this.status == 200) {
     // if response = warehouseStopped everything is ok
      currentWeight = 0 ;
      currA = 0;
      currB = 0;
      currC = 0;
      document.getElementById("A").innerHTML="0";
      document.getElementById("B").innerHTML="0";
      document.getElementById("C").innerHTML="0";
        }
};
xhttp.send();
document.getElementById('stop').disabled = false;
document.getElementById('resume').disabled = false;
document.getElementById("state").innerHTML="available";
document.getElementById("total").innerHTML="Total weight: "+(currentWeight)+"/100";

}
