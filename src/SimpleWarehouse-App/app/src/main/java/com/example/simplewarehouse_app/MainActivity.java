package com.example.simplewarehouse_app;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Set;
import java.util.UUID;

import handler.WarehouseHandler;
import okhttp3.*;
import thread.ConnectionManager;
import thread.ConnectionTask;

public class MainActivity extends AppCompatActivity {

    private TextView totalA;
    private TextView totalB;
    private TextView totalC;
    private TextView total;
    private Button btnA;
    private Button btnB;
    private Button btnC;
    private TextView connect;
    private TextView stateWarehouse;

    private ConnectionTask connTask;


    private static final String BT_TARGET_NAME = "modulobt06"; /*modulo HC-06*/
    private static final String APP_UUID = "00001101-0000-1000-8000-00805F9B34FB";
    private  BluetoothDevice targetDevice;
    private TextView textBt;

    private BluetoothAdapter btAdapter ;
    private final int ENABLE_BT_REQ = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btAdapter = BluetoothAdapter.getDefaultAdapter();

        if( btAdapter == null ){
            Log.e(" MyApp ","Bluetooth is not available on this device");
            Toast.makeText(getApplicationContext(),"Bluetooth is not available on this device",Toast.LENGTH_SHORT).show();
            finish();
        }
        if (!btAdapter.isEnabled() ){
            Toast.makeText(getApplicationContext(),"Bluetooh not enabled",Toast.LENGTH_SHORT).show();
            startActivityForResult ( new Intent( BluetoothAdapter.ACTION_REQUEST_ENABLE ),ENABLE_BT_REQ );
        }

        initUi();

        /*thread per inviare richieste get al server*/
        new Thread(new Runnable() {
            Handler h = new WarehouseHandler(Looper.getMainLooper(),totalA,totalB,totalC,btnA,btnB,btnC,total,stateWarehouse);

            @Override
            public void run() {
                while (true) {
                    int[] result = {0,0,0,0};
                    OkHttpClient client = new OkHttpClient();
                    String url = "http://ed360080.ngrok.io/toApp"; //path per le richieste
                    Request request = new Request.Builder()
                            .url(url)
                            .build();
                    try {
                        Response response = client.newCall(request).execute();
                        String jsonData = response.body().string();
                       // Log.d("ArrayPassato",jsonData);

                        JSONObject JObject = new JSONObject(jsonData);
                        if (JObject.has("A")) {
                            result[0] = JObject.getInt("A");
                        }
                        if (JObject.has("B")) {
                            result[1] = JObject.getInt("B");
                        }
                        if (JObject.has("C")) {
                            result[2] = JObject.getInt("C");
                        }

                        result[3] = JObject.getInt("STATE");

                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }

                    final Bundle bundle = new Bundle();
                    bundle.putIntArray(WarehouseHandler.NEW_WARE, result);
                    final Message msg = new Message();
                    msg.what = WarehouseHandler.UPDATE_WARE_MSG;
                    msg.setData(bundle);

                    h.sendMessage(msg);
                    try {
                        Thread.sleep(4000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }


    /*meccanismo di pairing*/
    @Override
    public void onActivityResult (int reqID , int res , Intent data ) {
        super.onActivityResult(reqID, res, data);
        if (reqID == ENABLE_BT_REQ && res == Activity.RESULT_OK) {
            Toast.makeText(getApplicationContext(),"Bluetooth enabled",Toast.LENGTH_SHORT).show();
        }
        if (reqID == ENABLE_BT_REQ && res == Activity.RESULT_CANCELED) {
        }
    }


    private void initUi(){
        totalA = findViewById(R.id.totalA);
        totalB = findViewById(R.id.totalB);
        totalC = findViewById(R.id.totalC);
        total =  findViewById(R.id.total_weight);
        btnA = findViewById(R.id.ware_A);
        btnB = findViewById(R.id.ware_B);
        btnC = findViewById(R.id.ware_C);
        textBt = findViewById(R.id.textBT);
        connect = findViewById(R.id.isConnected);
        stateWarehouse = findViewById(R.id.state);

        findViewById(R.id.buttonBT).setOnClickListener(v-> {


            if (!btAdapter.isEnabled() ){
                Toast.makeText(getApplicationContext(),"Restart app to avoid bug",Toast.LENGTH_SHORT).show();
                startActivityForResult ( new Intent( BluetoothAdapter.ACTION_REQUEST_ENABLE ),ENABLE_BT_REQ );
            }else {
                Set<BluetoothDevice> pairedList = btAdapter.getBondedDevices();
                if (pairedList.size() > 0) {
                    for (BluetoothDevice device : pairedList) {
                        if (device.getName().equals(BT_TARGET_NAME))
                            targetDevice = device;
                    }
                }

                UUID uuid = UUID.fromString(APP_UUID);
                connTask = new ConnectionTask(targetDevice, uuid);
                connTask.execute();
              /*non ritorna correttamente lo stato del getStatus()*/
             //   if(ConnectionManager.getInstance().getStatus()) {
                    btnA.setVisibility(View.VISIBLE);
                    btnB.setVisibility(View.VISIBLE);
                    btnC.setVisibility(View.VISIBLE);
                    textBt.setVisibility(View.INVISIBLE);

                findViewById(R.id.buttonBT).setVisibility(View.INVISIBLE);
                connect.setVisibility(View.VISIBLE);
              //  }
            }
        });


        btnA.setOnClickListener(v->{
            String buttonA = "A";
            ConnectionManager.getInstance().write(buttonA.getBytes());
        });

        btnB.setOnClickListener(v->{
            String buttonB = "B";
            ConnectionManager.getInstance().write(buttonB.getBytes());
        });

        btnC.setOnClickListener(v->{
            String buttonC = "C";
            ConnectionManager.getInstance().write(buttonC.getBytes());
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ConnectionManager.getInstance().cancel();
    }
}
