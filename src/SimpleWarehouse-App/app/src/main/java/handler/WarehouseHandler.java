package handler;


import android.content.res.Resources;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toolbar;

import androidx.annotation.NonNull;

import com.example.simplewarehouse_app.R;

import java.util.Arrays;

public class WarehouseHandler extends Handler {
    private final TextView state;
    private TextView ware_A;
    private TextView ware_B;
    private TextView ware_C;
    private Button btn_A;
    private Button btn_B;
    private Button btn_C;
    private TextView totalWeight;
    private  static final int deltaA = 10;
    private  static final int deltaB = 20;
    private  static final int deltaC = 30;
    private  static final int max = 100;
    private  static final int WRONG = 400;
    private  static final int OK = 200;
    public static final int UPDATE_WARE_MSG = 1;
    public static final String NEW_WARE = "new_ware";
    public  int prova = 0;

    public WarehouseHandler(@NonNull Looper looper, TextView ware_A, TextView ware_B, TextView ware_C,
                            Button btn_A, Button btn_B, Button btn_C,TextView tot,TextView state) {
        super(looper);
        this.ware_A = ware_A;
        this.ware_B = ware_B;
        this.ware_C = ware_C;
        this.btn_A = btn_A;
        this.btn_B = btn_B;
        this.btn_C = btn_C;
        this.totalWeight = tot;
        this.state = state;
    }

    @Override
    public void handleMessage(@NonNull Message msg) {

        switch (msg.what){
            case UPDATE_WARE_MSG:
                int[] total = msg.getData().getIntArray(NEW_WARE);
                int check = total[0]*deltaA+total[1]*deltaB+total[2]*deltaC;

                ware_A.setText(String.valueOf(total[0]));
                ware_B.setText(String.valueOf(total[1]));
                ware_C.setText(String.valueOf(total[2]));

                 if(total[3] == WRONG){
                     state.setText(R.string.stateNotAvailable);
                     btn_A.setEnabled(false);
                     btn_B.setEnabled(false);
                     btn_C.setEnabled(false);

                     if (check >= max) {
                         totalWeight.setText(R.string.Warehouse_full);
                     }
                 }else {
                     state.setText(R.string.stateAvailable);

                     if (check >= max) {
                         totalWeight.setText(R.string.Warehouse_full);
                         btn_A.setEnabled(false);
                         btn_B.setEnabled(false);
                         btn_C.setEnabled(false);
                     } else {
                         totalWeight.setText("Total weight: " + check + "/100");
                         btn_A.setEnabled(true);
                         btn_B.setEnabled(true);
                         btn_C.setEnabled(true);
                     }
                 }
                break;
        }
    }
}
