package thread;

import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ConnectionManager extends Thread {
    private BluetoothSocket btSocket;
    private InputStream btInStream;
    private OutputStream btOutStream;
    private boolean stop;
    private static ConnectionManager instance = null;
    private boolean status = false;

    private ConnectionManager(){
        stop = true ;
    }

    public boolean  getStatus(){
        return status;
    }

    public static ConnectionManager getInstance(){
        if(instance == null)
            instance = new ConnectionManager();
        return instance;
    }

    public void setChannel(BluetoothSocket socket){
        btSocket = socket;
        try {
            btInStream = socket.getInputStream();
            btOutStream = socket.getOutputStream();
        } catch ( IOException e) {
            e.printStackTrace();
        }
        status = true;
        stop = false ;
    }

    public void run(){
        byte[] buffer = new byte[1024];
        int nBytes = 0;
        while (!stop){
            try {
                nBytes = btInStream.read(buffer);
            } catch ( IOException e) {
                stop = true ;
            }
        }
    }

    public boolean write(byte[] bytes){
        if( btOutStream == null ) return false;
        try{
            btOutStream.write(bytes);
        } catch ( IOException e){ return false ;}
        return true ;
    }

    public void cancel(){
        try{
            btSocket.close();
        } catch ( IOException e){
            e.printStackTrace();
        }
    }
}
