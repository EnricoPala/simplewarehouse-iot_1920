package thread;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.IOException;
import java.util.UUID;

public class ConnectionTask extends AsyncTask<Void,Void,Void> {
    private BluetoothSocket btSocket = null;

    public ConnectionTask(BluetoothDevice device, UUID uuid) {
        try {
            btSocket = device.createRfcommSocketToServiceRecord(uuid);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    @Override
    protected Void doInBackground(Void ... params){
        try{
            btSocket.connect();
        } catch ( IOException connectException){
            try{
                btSocket.close();
            } catch (IOException closeException){
                closeException.printStackTrace();
            }
            return null ;
        }
        ConnectionManager cm = ConnectionManager.getInstance();
        cm.setChannel(btSocket);
       //
        // cm.start();
        return null ;
    }

    @Override
    protected void onPostExecute (Void par){ /* Connection active */ }
}